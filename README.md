# HEALTH CARE MODEL FOR IDENTIFICATION OF SPECIALIST PHYSICIANS AND ACCESSIBLE HOSPITALS #
A web based health care system for finding specialist physicians and their accessible hospitals 
done as a project work atthe University of Port Harcourt, Nigeria by Daniel E. Obasi

### Recommended ###

* PHP 5.6 or above
* MySql 5.6 or above

### How do I get set up? ###

* Install WAMP, LAMP or MAMP server or NGINX
* Be sure you have php and  Mysql installed
* Copy Project to your webserver document root
* Import Database.sql file into your MySQL
* Database configuration
* Database setups are in controler files
* Run after starting your server


### Who do I talk to? ###

* Repo owner or admin
* University of Port Harcourt Niigeria.