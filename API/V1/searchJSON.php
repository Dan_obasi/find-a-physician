<?php

require_once('../../database/dbconnect.php');

$sql = "SELECT * FROM doctors";
$result = $connection->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
//    while($row = $result->fetch_assoc()) {
//        echo "id: " . $row["id"]. " - Name: " . $row["firstname"]. " " . $row["lastname"]. "<br>";
//    }

    $doctors = mysqli_fetch_assoc($result);

   // echo json_encode($doctors);
    $response = array([
        "status"=>"200",
        "doctors"=>$doctors

    ]);

   // echo json_encode($response);
    echo json_encode($doctors);

} else {
    echo "No records found";
}
$conn->close();


?>