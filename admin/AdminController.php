<?php
/**
 * Created by PhpStorm.
 * User: danieleobasi
 * Date: 13/08/2017
 * Time: 1:00 PM
 */

namespace admin;


class AdminController
{
    //Doctor data fields
    private $doctorId;
    private $doctorFirstName;
    private $doctorLastName;
    private $doctorLicenseNumber;
    private $doctorEmail;
    private $doctorPhone;
    private $doctorSpecialization;
    private $doctorHospital;
    private $doctorState;
    private $doctorCountry = " ";

    //Hospital Fields
    private $hospitalId;
    private $hospitalState;
    private $hospitalCountry;
    private $hospitalName;
    private $hospitalAddress;
    private $hospitalWebsite;
    private $hospitalPhone;
    private $hospitalEmail;
    private $hospitalCoordinates;

    //Specialization Fields
    private $specializationId;
    private $specializationName;
    private $specializationDescription;
    private $specializationSpecialist;

    //Location Field
    private $locationName;

    //Illness Field
    private $illnessName;
    private $illnessDescription;

    //Database connection fields
    private  $DB_CONNECTION;
    private $databaseName = "find_a_physician";
    private $serverName = "localhost";
    private $databaseUserName = "root";
    private $databasePass = "eberechi2312";

    public function __construct()
    {
        $this->DB_CONNECTION = mysqli_connect($this->serverName,$this->databaseUserName,$this->databasePass,$this->databaseName);
    }
    function processDoctorData($data)
    {
        if (array_key_exists('doctor',$data))
            $this->doctorId = $data['doctor'];
        if (array_key_exists('first_name',$data))
            $this->doctorFirstName = $data['first_name'];
        if (array_key_exists('last_name',$data))
            $this->doctorLastName = $data['last_name'];
        if (array_key_exists('license_number',$data))
            $this->doctorLicenseNumber = $data['license_number'];
        if (array_key_exists('email',$data))
            $this->doctorEmail = $data['email'];
        if (array_key_exists('phone',$data))
            $this->doctorPhone = $data['phone'];
        if (array_key_exists('specialization',$data))
            $this->doctorSpecialization = $data['specialization'];
        if (array_key_exists('hospital',$data))
            $this->doctorHospital = $data['hospital'];
        if (array_key_exists('state',$data))
            $this->doctorState = $data['state'];
        if (array_key_exists('country',$data))
            $this->doctorCountry = $data['country'];

    }

    function processHospitalData($data)
    {
        if (array_key_exists('hospital',$data))
            $this->hospitalId = $data['hospital'];
        if (array_key_exists('state',$data))
            $this->hospitalState = $data['state'];
        if (array_key_exists('country',$data))
            $this->hospitalCountry = $data['country'];
        if (array_key_exists('name',$data))
            $this->hospitalName = $data['name'];
        if (array_key_exists('address',$data))
            $this->hospitalAddress = $data['address'];
        if (array_key_exists('phone',$data))
            $this->hospitalPhone = $data['phone'];
        if (array_key_exists('website',$data))
            $this->hospitalWebsite = $data['website'];
        if (array_key_exists('email',$data))
            $this->hospitalEmail = $data['email'];
        if (array_key_exists('coordinates',$data))
            $this->hospitalCoordinates = $data['coordinates'];

    }
    function processSpecializationData($data)
    {
        if (array_key_exists('specialization',$data))
            $this->specializationId = $data['specialization'];
        if (array_key_exists('name',$data))
            $this->specializationName = $data['name'];
        if (array_key_exists('description',$data))
            $this->specializationDescription = $data['description'];
        if (array_key_exists('specialist',$data))
            $this->specializationSpecialist = $data['specialist'];

    }
    function processLocationData($data)
    {
        if (array_key_exists('name',$data))
            $this->locationName = $data['name'];

    }
    function processIllnessData($data)
    {
        if (array_key_exists('name',$data))
            $this->illnessName = $data['name'];
        if (array_key_exists('description', $data))
            $this->illnessDescription = $data['description'];

    }
    public function addDoctor()
    {

        $sql = "INSERT INTO doctors (first_name,last_name,email,phone,doctor_license_number,hospital_id,specialization_id,
        state_id,country_id) VALUES ('$this->doctorFirstName','$this->doctorLastName','$this->doctorEmail',
        '$this->doctorPhone','$this->doctorLicenseNumber','$this->doctorHospital','$this->doctorSpecialization',
        '$this->doctorState','$this->$this->doctorCountry')";

        $result = mysqli_query($this->DB_CONNECTION,$sql);

        if ($result)
        {
            echo 'success';
        }else{ echo 'Not inserted';}


    }
    public function deleteDoctor($doctorId)
    {
        $sql = "DELETE FROM doctors WHERE (id = '$doctorId')";
        $result = mysqli_query($this->DB_CONNECTION, $sql);

        if ($result)
        {
            echo  'sucesss';
        }else{
            echo 'not deleted';
        }
        
    }
    public function updateDoctor()
    {
        $sql = "UPDATE doctors SET first_name = '$this->doctorFirstName', last_name = '$this->doctorLastName', 
      email = '$this->doctorEmail', phone = '$this->doctorPhone', doctor_license_number = '$this->doctorLicenseNumber'
         WHERE id = '$this->doctorId'";
        $result = mysqli_query($this->DB_CONNECTION,$sql);

        if ($result)
        {
            echo 'Update success';
        }else
        {
            echo 'update failed';

        }

    }
    public function addHospital()
    {


        $sql = "INSERT INTO hospitals (state_id,country_id,name,address,website,phone,email,
        coordinates) VALUES ('$this->hospitalState','$this->hospitalCountry','$this->hospitalName',
        '$this->hospitalAddress','$this->hospitalWebsite','$this->hospitalPhone','$this->hospitalEmail',
        '$this->hospitalCoordinates')";

        $result = mysqli_query($this->DB_CONNECTION,$sql);

        if ($result)
        {
            echo 'success';
        }else{ echo 'Not inserted';}


    }
    public function deleteHospital($hospitalId)
    {
        $sql = "DELETE FROM hospitals WHERE (id = '$hospitalId')";
        $result = mysqli_query($this->DB_CONNECTION, $sql);

        if ($result)
        {
            echo  'sucesss';
        }else{
            echo 'not deleted';
        }

    }
    public  function  updateHospital()
    {
        $sql = "UPDATE hospitals SET name = '$this->hospitalName', address = '$this->hospitalAddress', 
      website = '$this->hospitalWebsite', phone = '$this->hospitalPhone', email = '$this->hospitalEmail',
        coordinates = '$this->hospitalCoordinates' WHERE id = '$this->hospitalId'";
        $result = mysqli_query($this->DB_CONNECTION,$sql);

        if ($result)
        {
            echo 'Update success';
        }else
            {
                echo 'update failed';

            }

    }
    public function addSpecialization()
    {


        $sql = "INSERT INTO specializations (name,description,specialist) VALUES ('$this->specializationName',
           '$this->specializationDescription', '$this->specializationSpecialist')";

        $result = mysqli_query($this->DB_CONNECTION,$sql);

        if ($result)
        {
            echo 'success';
        }else{ echo 'Not inserted';}


    }
    public  function  updateSpecialization()
    {
        $sql = "UPDATE specializations SET name = '$this->specializationName', description = '$this->specializationDescription', 
        specialist = '$this->specializationSpecialist' WHERE id = '$this->specializationId'";
        $result = mysqli_query($this->DB_CONNECTION,$sql);

        if ($result)
        {
            echo 'Update success';
        }else
        {
            echo 'update failed';

        }

    }
    public function addLocation()
    {


        $sql = "INSERT INTO states (name) VALUES ('$this->locationName')";

        $result = mysqli_query($this->DB_CONNECTION,$sql);

        if ($result)
        {
            echo 'success';
        }else{ echo 'Not inserted';}


    }
 public function addIllness()
    {


        $sql = "INSERT INTO illness (name, description) VALUES ('$this->illnessName', '$this->$this->illnessDescription')";

        $result = mysqli_query($this->DB_CONNECTION,$sql);

        if ($result)
        {
            echo 'success';
        }else{ echo 'Not inserted';}


    }

    public function getLocations()
    {
        $sql = "SELECT * FROM states";
        $result = mysqli_query($this->DB_CONNECTION,$sql);
        return $result;

    }
    public function getAdmins()
    {
        $sql = "SELECT first_name,last_name, email FROM admins";
        $result = mysqli_query($this->DB_CONNECTION,$sql);
        return $result;

    }
    public function getIllness()
    {
        $sql = "SELECT * FROM illness";
        $result = mysqli_query($this->DB_CONNECTION,$sql);
        return $result;

    }
    public function getHospitals()
    {
        $sql = "SELECT hospitals.id as id, hospitals.name as name, hospitals.address as address, hospitals.website as website, hospitals.phone as phone,
        hospitals.email as email, hospitals.coordinates as coordinates, states.name as state, countries.name as country FROM ((hospitals INNER JOIN states ON hospitals.state_id = states.id)
       INNER JOIN countries ON hospitals.country_id = countries.id)";

        $result = mysqli_query($this->DB_CONNECTION,$sql);
        return $result;

    }
    public function getSpecializations()
    {
        $sql = "SELECT * FROM specializations";
        $result = mysqli_query($this->DB_CONNECTION,$sql);
        return $result;

    }
    public function getDoctors()
    {
        $sql = "SELECT doctors.id as id, doctors.first_name as firstName, doctors.last_name as lastName, doctors.doctor_license_number,doctors.phone,doctors.email, hospitals.name as hospital, hospitals.phone as hospitalPhone, 
        specializations.name as specialization FROM  ((doctors INNER JOIN hospitals ON doctors.hospital_id  = hospitals.id)
         INNER JOIN specializations ON doctors.specialization_id = specializations.id)";
         
        $result = mysqli_query($this->DB_CONNECTION,$sql);
        return $result;

    }


}