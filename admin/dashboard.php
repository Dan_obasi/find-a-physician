<?php
/**
 * Created by PhpStorm.
 * User: danieleobasi
 * Date: 12/08/2017
 * Time: 12:58 PM
 */
session_start();
if ( ! isset($_SESSION['user'])){
    header('Location: index.php');
    exit();

}



require_once('AdminController.php');
use  admin\AdminController;

$adminController = new AdminController;
//Get locations
$locations = $adminController->getLocations();

//Get Admins
$admins =  $adminController->getAdmins();

//Get Illness
$illness =  $adminController->getIllness();

//Get Specialition
$specializations =  $adminController->getSpecializations();

//Get Hospitals
$hospitals =  $adminController->getHospitals();

//Get Doctors
$doctors =  $adminController->getDoctors();


?>
<!DOCTYPE html>
<html>
<head>
    <title> Admin Dashboard</title>
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="../assets/css/card.css"/>
</head>
<body>
<div class="container-fluid" style="max-width: 80%;padding-top: 1%">
    <div class="page-header">
        <h1>Dashboard <small>Administrator's Dashboard</small></h1>
    </div>
    <div class="col-md-4" align="center">

            <div class="card" style="padding: 2%"data-toggle="modal" data-target=".bs-doctors-modal-lg">
                <div class="row">
                    <div class="col-sm-8">
                        <h2> Doctors</h2>
                    </div>
                    <div class="col-sm-4 ">
                        <h1 class="glyphicon glyphicon-user " ></h1>

                    </div>
                </div>


            </div>

    </div>
    <div class="col-md-4" align="center">
            <div class="card" style="padding: 2%" data-toggle="modal" data-target=".bs-hospitals-modal-lg">
                <div class="row">
                    <div class="col-sm-8">
                        <h2> Hospitals</h2>
                    </div>
                    <div class="col-sm-4 ">
                        <h1 class="glyphicon glyphicon-plus " ></h1>

                    </div>
                </div>
            </div>

    </div>
    <div class="col-md-4" align="center">
            <div class="card" style="padding: 2%" data-toggle="modal" data-target=".bs-specializations-modal-md">
                <div class="row">
                    <div class="col-sm-8">
                        <h2>Specializations</h2>
                    </div>
                    <div class="col-sm-4 ">
                        <h1 class="glyphicon glyphicon-book " ></h1>

                    </div>
                </div>
            </div>

    </div><br><br><br><br><br>
    <div class="col-md-4" align="center">
            <div class="card" style="padding: 2%" data-toggle="modal" data-target=".bs-locations-modal-md">
                <div class="row">
                    <div class="col-sm-8">
                        <h2>Locations</h2>
                    </div>
                    <div class="col-sm-4 ">
                        <h1 class="glyphicon glyphicon-map-marker " ></h1>

                    </div>
                </div>
            </div>

    </div>
    <div class="col-md-4" align="center">
            <div class="card" style="padding: 2%" data-toggle="modal" data-target=".bs-admins-modal-lg">
                <div class="row">
                    <div class="col-sm-8">
                        <h2>Admins</h2>
                    </div>
                    <div class="col-sm-4 ">
                        <h1 class="glyphicon glyphicon-user " ></h1>

                    </div>
                </div>
            </div>

    </div>
    <div class="col-md-4" align="center">
            <div class="card" style="padding: 2%" data-toggle="modal" data-target=".bs-illness-modal-md">
                <div class="row">
                    <div class="col-sm-8">
                        <h2>illness</h2>
                    </div>
                    <div class="col-sm-4 ">
                        <h1 class="glyphicon glyphicon-plus-sign" ></h1>

                    </div>
                </div>
            </div>

    </div><br><br><br><br><br>
    <div class="col-md-4" align="center">
        <a>
            <div class="card" style="padding: 2%">
                <div class="row">
                    <div class="col-sm-8">
                        <h2>Logout</h2>
                    </div>
                    <div class="col-sm-4 ">
                        <h1 class="glyphicon glyphicon-off" ></h1>

                    </div>
                </div>
            </div>

        </a>


    </div>
</div>
<!-- Doctors Modal-->
<div class="modal fade bs-doctors-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="container-fluid ">
                <h3>Doctors</h3>
                <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseAddDoctor" aria-expanded="false" aria-controls="collapseAddDoctor">
                    Add Doctor
                </button>
                <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseRequest" aria-expanded="false" aria-controls="collapseRequest">
                    Request
                </button><br><br>
                <div class="collapse" id="collapseAddDoctor">
                    <div class="well">
                        <form action="AdminRequestHandler.php" role="form" method="post" >
                            <div class="form-group "align="center">
                                <input type="text" name="first_name" placeholder="Doctor's First Name" required autofocus>
                                <input type="text" name="last_name" placeholder="Doctor's Last Name" required >
                                <input type="text" name="license_number" placeholder="Doctor's License Number" required >
                                <input type="text" name="phone" placeholder="Doctor's phone number" required >
                                <input type="email" name="email" placeholder="Doctor's email address" required >
                                <input type="hidden" name="type" value="add_doctor">
                                <input type="hidden" name="country" value="1">
                            </div>

                            <div class="form-group" align="center">

                                <select name="specialization" required >
                                    <option disabled selected value> -- select doctor's specialization -- </option>
                                    <?php
                                    foreach ($specializations as $specialization)
                                        echo "<option value='{$specialization['id']}'>{$specialization['name']}</option>"
                                    ?>
                                </select>
                                <select name="hospital" required>
                                    <option disabled selected value> -- select doctor's hospital -- </option>
                                    <?php
                                    foreach ($hospitals as $hospital)
                                        echo "<option value='{$hospital['id']}'>{$hospital['name']}</option>"
                                    ?>
                                </select>
                                <select name="state" required>
                                    <option disabled selected value> -- select doctor's state-- </option>
                                    <?php
                                    foreach ($locations as $location)
                                        echo "<option value='{$location['id']}'>{$location['name']}</option>"
                                    ?>

                                </select>

                            </div>
                            <div align="center"><input type="submit" value="save" class="btn btn-success"></div>
                        </form>
                    </div>
                </div>
                <div class="collapse" id="collapseRequest">
                    <div class="well">
                        No New request
                    </div>
                </div>
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Specialization</th>
                        <th>Hospital</th>
                        <th>License No.</th>
                        <th>email</th>
                        <th>phone</th>
                        <th>Action</th>
                    </tr>

                    </thead>
                    <tbody>
                    <?php
                    foreach ($doctors as $doctor)
                        echo "
                          <tr>
                            <td>{$doctor['firstName']}</td>
                            <td>{$doctor['lastName']}</td>
                            <td>{$doctor['specialization']}</td>
                            <td>{$doctor['hospital']}</td>
                            <td>{$doctor['doctor_license_number']}</td>
                            <td>{$doctor['email']}</td>
                            <td>{$doctor['phone']}</td>
                            <td>
                            <i class='glyphicon glyphicon-edit' data-toggle='collapse' data-target='#doc{$doctor['id']}' aria-expanded='false' aria-controls='doc{$doctor['id']}'></i>&nbsp;&nbsp;
                            <i class='glyphicon glyphicon-trash' data-toggle='collapse' data-target='#doc{$doctor['id']}del' aria-expanded='false' aria-controls='doc{$doctor['id']}del'></i>&nbsp;&nbsp;
                         
                            </td>
                    </tr>
                    <div class='collapse' id='doc{$doctor['id']}'>
                        <div class='well'>
                                  <form action=\"AdminRequestHandler.php\" role=\"form\" method=\"post\" >
                                    <div class=\"form-group \"align=\"center\">
                                        <input type=\"text\" name=\"first_name\" placeholder=\"Doctor's First Name\" required value='{$doctor['firstName']}' >
                                        <input type=\"text\" name=\"last_name\" placeholder=\"Doctor's Last Name\" value='{$doctor['lastName']}' required >
                                        <input type=\"text\" name=\"license_number\" placeholder=\"Doctor's License Number\" value='{$doctor['doctor_license_number']}' required >
                                        <input type=\"text\" name=\"phone\" placeholder=\"Doctor's phone number\" value='{$doctor['phone']}' required >
                                        <input type=\"email\" name=\"email\" placeholder=\"Doctor's email address\" value='{$doctor['email']}' required >
                                        <input type=\"hidden\" name=\"type\" value=\"update_doctor\">
                                        <input type=\"hidden\" name=\"doctor\" value=\"{$doctor['id']}\">
                                        <input type=\"hidden\" name=\"country\" value=\"1\">
                                    </div>
        
                                    
                                    <div align=\"center\"><input type=\"submit\" value=\"save\" class=\"btn btn-success\">
                                    <i class='btn btn-danger' data-toggle='collapse' data-target='#doc{$doctor['id']}' aria-expanded='false' aria-controls='{$doctor['id']}'>Close</i> </div>
                            
                                </form>
                        
                        </div>
                    </div> 
                    <div class='collapse' id='doc{$doctor['id']}del'>
                        <div class='well'>
                        Are you sure you want to delete {$doctor['firstName']} {$doctor['lastName']} ?<br><br>
                        <form action='AdminRequestHandler.php' method='post'>
                         <input type='hidden' name='doctor' value='{$doctor['id']}'>
                         <input type='hidden' name='type' value='delete_doctor'>
                         <input class='btn btn-success' type='submit' value='Yes'>
                          <i class='btn btn-danger' data-toggle='collapse' data-target='#doc{$doctor['id']}del' aria-expanded='false' aria-controls='{$doctor['id']}'>No</i> </div>
                        
                        </form>
                        
                        
                        </div>
                    </div>
                        "
                    ?>


                    </tbody>
                </table>


            </div>

        </div>
    </div>
</div>
<!-- Doctors Modal-->
<!-- Hospitals Modal-->
<div class="modal fade bs-hospitals-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="container-fluid ">
                <h3>Hospitals</h3>
                <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseAddHospital" aria-expanded="false" aria-controls="collapseAddHospital">
                    Add Hospital
                </button><br><br>
                <div class="collapse" id="collapseAddHospital">
                    <div class="well">
                        <form action="AdminRequestHandler.php" role="form" method="post" >
                            <div class="form-group "align="center">
                                <input type="text" name="name" placeholder="Hospital Name" required autofocus>
                                <input type="text" name="website" placeholder="website URL" >
                                <input type="text" name="phone" placeholder="Phone Number" required >
                                <input type="email" name="email" placeholder="Email Address"  >
                                <input type="text" name="coordinates" placeholder="GPS coordinates" required >
                                <input type="hidden" name="type" value="add_hospital">
                                <input type="hidden" name="country" value="1">
                            </div>


                            <div class="row" align="center">
                                <div class="col-sm-6">
                                     <textarea class="form-control" name="address" rows="4" cols="50">

                                </textarea>

                                </div>
                                <div class="col-sm-6">

                                    <select class="form-control" name="state" required>
                                        <option disabled selected value> -- select Hospital's location-- </option>
                                        <?php
                                        foreach ($locations as $location)
                                            echo "<option value='{$location['id']}'>{$location['name']}</option>"
                                        ?>

                                    </select>

                                </div><br>

                            </div><br>
                            <div align="center"><input type="submit" value="save" class="btn btn-success"></div>
                        </form>
                    </div>
                </div>
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Address</th>
                        <th>website</th>
                        <th>phone</th>
                        <th>email</th>
                        <th>state</th>
                        <th>country</th>
                        <th>coordinates</th>

                    </tr>

                    </thead>
                    <tbody>
                    <?php
                    foreach ($hospitals as $hospital)
                        echo "<tr>
                                    <td>{$hospital['name']}</td>
                                    <td>{$hospital['address']}</td>
                                    <td>{$hospital['website']}</td>
                                    <td>{$hospital['phone']}</td>
                                    <td>{$hospital['email']}</td>
                                    <td>{$hospital['state']}</td>
                                    <td>{$hospital['country']}</td>
                                    <td>{$hospital['coordinates']}</td>
                                    <td><i class=\"glyphicon glyphicon-edit\" type=\"button\" data-toggle=\"collapse\" data-target=\"#hos{$hospital['id']}\" aria-expanded=\"false\" aria-controls=\"hos{$hospital['id']}\"></i></td>
                                   <!-- <i class=\"glyphicon glyphicon-trash\" type=\"button\" data-toggle=\"collapse\" data-target=\"#hos{$hospital['id']}del\" aria-expanded=\"false\" aria-controls=\"hos{$hospital['id']}del\"></i></td>-->
                                </tr>
                                <div class='collapse' id='hos{$hospital['id']}'>
                                        <div class='well'> 
                                       <div align='center'> <h3>Update Data</h3></div>
                                             <form action=\"AdminRequestHandler.php\" role=\"form\" method=\"post\" >
                                                    <div class=\"form-group \"align=\"center\">
                                                    <div class='col-md-7'>
                                                     <input class='form-control' type=\"text\" name=\"name\" placeholder=\"{$hospital['name']}\" value='{$hospital['name']}' required autofocus>
                                                    </div>
                                                    <div class='col-md-5'>
                                                     <input class='form-control' type=\"text\" name=\"website\" placeholder=\"website URL\" value='{$hospital['website']}'>
                                                    </div><br><br>
                                                    <div class='col-md-4'>
                                                      <input class='form-control' type=\"text\" name=\"phone\" placeholder=\"Phone Number\" value='{$hospital['phone']}' required >
                                                    </div>
                                                    <div class='col-md-4'>
                                                    <input class='form-control' type=\"email\" name=\"email\" placeholder=\"Email Address\"  value='{$hospital['email']}'>
                                                    </div>
                                                    <div class='col-md-4'>
                                                          <input class='form-control' type=\"text\" name=\"coordinates\" placeholder=\"GPS coordinates\" value='{$hospital['coordinates']}' required >
                                                    
                                                    </div><br><br>
                                                     
                                        
                                                        <input type=\"hidden\" name=\"type\" value=\"update_hospital\">
                                                        <input type=\"hidden\" name=\"hospital\" value=\"{$hospital['id']}\">
                                                         <div class=\"col-sm-12\">
                                                                 <textarea class=\"form-control\" name=\"address\" rows=\"4\" cols=\"50\">{$hospital['address']}
                                                              
                            
                                                                  </textarea><br>
                            
                                                          </div>
                                                          

                                                
                                                    </div>
                        
                        
                                                   
                                                    <div align=\"center\"><input type=\"submit\" value=\"save\" class=\"btn btn-success\">
                                                    <i class='btn btn-danger' data-toggle='collapse' data-target='#hos{$hospital['id']}' aria-expanded='false' aria-controls='hos{$hospital['id']}'>Close</i>
                                                    </div>
                                                 </form>
                                                                      
                                        </div>
                                </div>
                                 <div class='collapse' id='hos{$hospital['id']}del'>
                                   <div class='well'>
                                     Are you sure you want ro delete {$hospital['name']}?<br>
                                     <form method='post' role='form' action='AdminRequestHandler.php'>
                                       <input type='hidden' name='hospital_id' value='{$hospital['id']}'>
                                       <input type='hidden' name='type' value='delete_hospital'>
                                        <input class='btn btn-success' type='submit' value='Yes'>
                                      </form>
                                    
                                     <button class='btn btn-danger' data-toggle='collapse' data-target='#{$hospital['id']}del'>No</button>
                                   </div>
                                 
                                </div> "
                    ?>

                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>
<!-- Hospitals Modal-->
<!-- Specializations Modal-->
<div class="modal fade bs-specializations-modal-md" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="container-fluid ">
                <h3>Specialization</h3>
                <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseAddSpecialization" aria-expanded="false" aria-controls="collapseAddSpecialization">
                    Add Specialization
                </button><br><br>
                <div class="collapse" id="collapseAddSpecialization">
                    <div class="well">
                        <form action='AdminRequestHandler.php' role='form' method='post'>
                            <input type="hidden" name="type" value="add_specialization">
                            <input class="form-control" type="text" name="name" placeholder="Name of Specialization" required><br>
                            <input class="form-control" type="text" name="specialist" placeholder="Name of Specialist" required><br>
                            Description
                            <textarea class="form-control" name="description" required>

                                    </textarea><br>
                            <div align="center"><input class="btn btn-success" type="submit" value="Save"></div>
                        </form>
                    </div>
                </div>
                <div class="list-group">
                    <?php
                    foreach ($specializations as $specialization)
                        echo "<div class='list-group-item'>
                                <div class='container-fluid'>
                                 <strong class='list-group-item-heading'>{$specialization['name']}</strong>
                              
                                 <p class='list-group-item-text'>{$specialization['description']}. A physcian in this field is known as <strong>{$specialization['specialist']}</strong></p>
                                 <i class='pull-right glyphicon glyphicon-edit' data-toggle='collapse' data-target='#spe{$specialization['id']}' aria-expanded='false' aria-controls='spe{$specialization['id']}'>Edit</i>
                                
                                </div>
                                  
                                </div>
                                <div class='collapse' id='spe{$specialization['id']}'>
                                  <div class='well'>
                                    <form action='AdminRequestHandler.php' role='form' method='post'>
                                        <input type='hidden' name='specialization' value='{$specialization['id']}'>
                                        <input type='hidden' name='type' value='update_specialization'>
                                        <input class='form-control' type='text' name='name' placeholder='Name of Specialization' value='{$specialization['name']}' required><br>
                                        <input class='form-control' type='text' name='specialist' placeholder='Name of Specialist' value='{$specialization['specialist']}' required><br>
                                        Description
                                        <textarea class='form-control' name='description' required>{$specialization['description']}
                                        
                                    </textarea><br>
                                   <div align='center'><input class='btn btn-success' type='submit'value='Save'></div> 
                                    </form>
                                  
                                  </div>
                                
                                </div> "?>

                </div>
                <!--<table class="table table-hover">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Description</th>
                    </tr>

                    </thead>
                    <tbody>
                    <?php
                    foreach ($specializations as $specialization)
                        echo "<tr><td>{$specialization['name']}</td>
                        <td>{$specialization['description']}</td></tr>"
                    ?>

                    </tbody>
                </table>-->

            </div>
        </div>
    </div>
</div>
<!-- Specializations Modal-->
<!-- Locations Modal-->
<div class="modal fade bs-locations-modal-md" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="container-fluid ">
                <h3>Locations</h3>
                <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseAddState" aria-expanded="false" aria-controls="collapseAddState">
                    Add State
                </button><br><br>
                <div class="collapse" id="collapseAddState">
                    <div class="well">
                        <form action="AdminRequestHandler.php" role="form" method="post">
                            <input type="hidden" name="type" value="add_location">
                            <div class="input-group">
                                <input type="text" class="form-control" name="name" placeholder="Name">
                                <span class="input-group-btn">
                                    <input class="btn btn-success" type="submit" value="Save">
                                  </span>
                            </div>

                        </form>
                    </div>
                </div>
                <div class="list-group">
                    <?php foreach ($locations as $location)

                        echo "<div class='list-group-item'>{$location['name']}</div>";?>

                </div>


            </div>
        </div>
    </div>
</div>
<!-- Locations Modal-->
<!-- Admins Modal-->
<div class="modal fade bs-admins-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="container-fluid ">
                <h3>Administrators</h3>
                <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseAddAdmin" aria-expanded="false" aria-controls="collapseAddAdmin">
                    Add Administrator
                </button><br><br>
                <div class="collapse" id="collapseAddAdmin">
                    <div class="well">
                        Add new Admin
                    </div>
                </div>
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th> First Name</th>
                        <th>Last Name</th>
                        <th>email</th>
                    </tr>

                    </thead>
                    <tbody>
                    <?php
                    foreach ($admins as $admin)
                        echo "<tr><td>{$admin['first_name']}</td><td>{$admin['last_name']}</td><td>{$admin['email']}</td></tr>"
                    ?>


                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>
<!-- Admins Modal-->
<!-- Illness Modal-->
<div class="modal fade bs-illness-modal-md" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="container-fluid ">
                <h3>Illness</h3>
                <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseAddIllness" aria-expanded="false" aria-controls="collapseAddIllness">
                    Add illness
                </button><br><br>
                <div class="collapse" id="collapseAddIllness">
                    <div class="well" align="center">
                        <form action="AdminRequestHandler.php" role="form" method="post">
                            <input type="hidden" name="type" value="add_illness"><br/>
                            <div class="input-group" align="center">
                                <input type="text" class="form-control" name="name" placeholder="Name">
                                <textarea class="form-control">Description</textarea><br/><br/><br/>
                                <div>
                                    <input class="btn btn-success" type="submit" value="Save">
                                </div>


                            </div>

                        </form>
                    </div>
                </div>

                    <?php
                    foreach ($illness as $illness)
                        echo " <div class=\"list-group\">
                    <div class=\"list-group-item\">
                        <div class=\"container-fluid\">
                            <strong class=\"list-group-item-heading\">{$illness['name']}</strong>
                            <p class=\"list-group-item-text\">{$illness['description']}</p>
                        </div>
                    </div>
                </div>"
                    ?>





            </div>
        </div>
    </div>
</div>
<!-- Illness Modal-->

<script src="../assets/js/jquery-3.2.0.min.js"></script>

<script src="../assets/js/bootstrap.min.js" ></script>
<script scr="../assets/js/main.js"></script>
</body>
</html>
