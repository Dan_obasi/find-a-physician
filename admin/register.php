<?php
/**
 * Created by PhpStorm.
 * User: danieleobasi
 * Date: 12/08/2017
 * Time: 10:56 AM
 */
include_once ('Admin.php');
include_once ('Authentication.php');
session_start();
$_SESSION['error'] = array();

$auth = new \admin\Authentication();
$auth->prepareData($_POST);

$adminStatus = $auth->checkAdmin();

if ($adminStatus === false)
{
    $admin = new \admin\Admin();
    $admin->prepareData($_POST);
    $admin->createNewAdmin();


}else{

    $_SESSION['error']='User already exist';
}