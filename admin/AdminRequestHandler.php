<?php
/**
 * Created by PhpStorm.
 * User: danieleobasi
 * Date: 13/08/2017
 * Time: 12:59 PM
 */
include_once ('AdminController.php');
use  admin\AdminController;

$adminController = new AdminController();
//$adminController->processDoctorData($_POST);
if ($_POST['type']=='add_doctor')
{
//echo 'here';
}
switch ($_POST['type']){
    case 'add_doctor':
        $adminController->processDoctorData($_POST);
        $adminController->addDoctor();
        break;
    case 'update_doctor':
        $adminController->processDoctorData($_POST);
        $adminController->updateDoctor();
        break;
    case 'delete_doctor':
        $adminController->deleteDoctor($_POST['doctor']);
        break;
    case 'add_hospital':
        $adminController->processHospitalData($_POST);
        $adminController->addHospital();
        break;
    case 'add_illness':
        $adminController->processIllnessData($_POST);
        $adminController->addIllness();
        break;
    case 'delete_hospital':
        $adminController->deleteHospital($_POST['hospital_id']);
        break;
    case 'update_hospital':
        $adminController->processHospitalData($_POST);
        $adminController->updateHospital();
        break;
    case 'add_specialization':
        $adminController->processSpecializationData($_POST);
        $adminController->addSpecialization();
        break;
    case 'update_specialization':
        $adminController->processSpecializationData($_POST);
        $adminController->updateSpecialization();
        break;
    case 'add_location':
        $adminController->processLocationData($_POST);
        $adminController->addLocation();
        break;
    default:
        echo'unknown';
}

