<?php
/**
 * Created by PhpStorm.
 * User: danieleobasi
 * Date: 12/08/2017
 * Time: 10:56 AM
 */
include_once ('Authentication.php');
use  admin\Authentication;
session_start();
$_SESSION['error'] = array();

$auth = new Authentication();
$auth->prepareData($_POST);

$adminStatus = $auth->checkLogin();

if ($adminStatus == true )
{

    $_SESSION['user'] = $_POST['email'];

    header('Location: http:findaphysician:8888/admin/dashboard.php');

}else
    {
        echo 'invalid login';
        header('Location: '. $_SERVER['HTTP_REFERER']);
        //Login Invalid

        $_SESSION['error'] = 'email or password invalid';
    }