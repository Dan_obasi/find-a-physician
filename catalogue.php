<?php
//require_once('database/dbconnect.php');
require_once('guest/GuestController.php');
use  guest\GuestController;

$guestController = new GuestController;
$locations = $guestController->getLocations();
$doctorCatalogue = $guestController->getCatalogue();
$specializations = $guestController->getSpecializations();
?>
<!DOCTYPE html>
<html>
<head>
    <title> Catalogue| Find A physician</title>
    <link rel="stylesheet" href="assets/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="assets/css/card.css"/>


</head>
<body>

<div align="center" style="padding-top: 30px">

    <nav class="navbar navbar-inverse navbar-fixed-top" >
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="/">Find A Physician</a>
            </div>
            <ul class="nav navbar-nav">
                <li ><a href="index.php">QuickSearch</a></li>
                <li class="active"><a href="/catalogue.php">Catalogue</a></li>
                <li class=""><a href="/doctors">For Doctors</a></li>
                <!--<li><a href="#">Page 1</a></li>-->

            </ul>
            <ul class="nav navbar-nav navbar-right">
               <!-- <li><a href="#"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>-->
                <li><a href="/admin/index.php"><span class="glyphicon glyphicon-log-in"></span> Admin</a></li>
            </ul>
        </div>
    </nav>
</div>

<div class="container" style="padding-top: 5px">
    <div class="page-header">
        <h1>Catalogue <small>All Specialist Physicians</small></h1>
     
    </div>

       <div class="dropdown col-lg-2">
                <button class="btn btn-default"id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Filter by Specialization
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" aria-labelledby="dLabel">
                    <?php
                      foreach($specializations as $specialization)
                      echo 
                      "<li><a href='?specialization={$specialization['specialist']}'>{$specialization['specialist']}</a></li>"
                      ?>
                </ul>
        </div>
        <div class="dropdown col-lg-2">
                <button class="btn btn-default"id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                   Filter by  Location
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" aria-labelledby="dLabel">
                <?php
                foreach($locations as $location)
                 if(isset($_GET['specialization'])){
                    echo 
                    "<li><a href='?specialization={$_GET['specialization']}&location={$location['id']}'>{$location['name']}</a></li>";

                 }elseif(isset($_GET['location'])){
                     
                   echo "<li><a href='?location={$location['id']}'>{$location['name']}</a></li>";
                 }else{
                    echo "<li><a href='?location={$location['id']}'>{$location['name']}</a></li>";
                 }
                      
                      ?>
                </ul>
        </div>
         <div class="col-lg-2">
             <a class="btn btn-default" href="catalogue.php"> Show All</a>

         </div><br><br>




        
    <table class="table table-hover table-bordered">
                    <thead>
                    <tr>
                        <th>SPECIALIZATION</th>
                        <th>DOCTOR</th>
                        <th>HOSPITAL</th>
                    </tr>

                    </thead>
                    <tbody>
                    <?php
                        foreach($doctorCatalogue as $catalogue)
                        echo"
                        <tr>
                            <td><strong>{$catalogue['specialization']}</strong><br>
                            <small>{$catalogue['description']}</small></td>
                            <td>{$catalogue['firstName']} {$catalogue['lastName']}</td>
                            <td><strong>{$catalogue['hospital']}</strong><br>
                            {$catalogue['address']}<br>
                            <small><strong>Website:</strong> <a href='{$catalogue['website']}'>{$catalogue['website']}</a></small>
                            <small><strong>email:</strong> {$catalogue['email']}</small><br>
                            <small><strong>Phone:</strong> {$catalogue['hospitalPhone']}</small><br>
                            <small><strong>Locate:</strong> <a class='glyphicon glyphicon-map-marker' href='http://www.google.com/maps/place/{$catalogue['coordinates']}'>Map</a></small>
                            </td>
                        </tr>"
                     ?>
                    </tbody>
        </table>
                

    <div class=""  align="center">

          
         

   </div><!-- /.row -->
      
</div>







<script src="assets/js/jquery-3.2.0.min.js"></script>

<script src="assets/js/bootstrap.min.js" ></script>
<script scr="assets/js/main.js"></script>
<script>

</script>
</body>



</html>