<?php
/**
 * Created by PhpStorm.
 * User: danieleobasi
 * Date: 14/08/2017
 * Time: 7:36 AM
 */

namespace guest;


class GuestController
{
    //Database connection fields
    private  $DB_CONNECTION;
    private $databaseName = "find_a_physician";
    private $serverName = "localhost";
    private $databaseUserName = "root";
    private $databasePass = "eberechi2312";

    public function __construct()
    {
        $this->DB_CONNECTION = mysqli_connect($this->serverName,$this->databaseUserName,$this->databasePass,$this->databaseName);
    }

    public function getLocations()
    {
        $sql="SELECT * FROM states";
        $result = mysqli_query($this->DB_CONNECTION,$sql);
        return $result;

    }
    public function getCatalogue()
    {
        if(isset($_GET['specialization']) && !isset($_GET['location'])){
            $spec = $_GET['specialization'];

            $sql="SELECT doctors.first_name as firstName, doctors.last_name as lastName, hospitals.name as hospital, hospitals.phone as hospitalPhone, 
            hospitals.address as address, hospitals.website as website, hospitals.email as email, hospitals.coordinates as coordinates,
            specializations.description as description , specializations.specialist   as specialization 
            FROM  ((doctors INNER JOIN hospitals ON doctors.hospital_id  = hospitals.id)
             INNER JOIN specializations ON doctors.specialization_id = specializations.id) WHERE (specializations.specialist) = '$spec'ORDER BY specialization ASC, firstName ASC";
        }elseif (! isset($_GET['specialization']) && isset($_GET['location'])){
            $location = $_GET['location'];

            $sql="SELECT doctors.first_name as firstName, doctors.last_name as lastName, hospitals.name as hospital, hospitals.phone as hospitalPhone, 
            hospitals.address as address, hospitals.website as website, hospitals.email as email, hospitals.coordinates as coordinates,
            specializations.description as description , specializations.specialist   as specialization 
            FROM  ((doctors INNER JOIN hospitals ON doctors.hospital_id  = hospitals.id)
             INNER JOIN specializations ON doctors.specialization_id = specializations.id) WHERE (hospitals.state_id) = '$location'ORDER BY specialization ASC, firstName ASC";

        }elseif(isset($_GET['specialization']) && isset($_GET['location'])){
            $spec = $_GET['specialization'];
            $location = $_GET['location'];

            $sql="SELECT doctors.first_name as firstName, doctors.last_name as lastName, hospitals.name as hospital, hospitals.phone as hospitalPhone, 
            hospitals.address as address, hospitals.website as website, hospitals.email as email, hospitals.coordinates as coordinates,
            specializations.description as description , specializations.specialist   as specialization 
            FROM  ((doctors INNER JOIN hospitals ON doctors.hospital_id  = hospitals.id)
             INNER JOIN specializations ON doctors.specialization_id = specializations.id) WHERE (hospitals.state_id) = '$location' AND (specializations.specialist) = '$spec' ORDER BY specialization ASC, firstName ASC";


        }
        else{
        $sql="SELECT doctors.first_name as firstName, doctors.last_name as lastName, hospitals.name as hospital, hospitals.phone as hospitalPhone, 
        hospitals.address as address, hospitals.website as website, hospitals.email as email, hospitals.coordinates as coordinates,
        specializations.description as description , specializations.specialist   as specialization 
        FROM  ((doctors INNER JOIN hospitals ON doctors.hospital_id  = hospitals.id)
         INNER JOIN specializations ON doctors.specialization_id = specializations.id) ORDER BY specialization ASC, firstName ASC";}
        $result = mysqli_query($this->DB_CONNECTION,$sql);
        return $result;

    }
    public function getSpecializations()
    {
       $sql="SELECT * FROM specializations";
        $result = mysqli_query($this->DB_CONNECTION,$sql);
        return $result;

    }

}