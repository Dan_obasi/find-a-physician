<?php
require_once('database/dbconnect.php');
require_once('guest/GuestController.php');
use  guest\GuestController;

$guestController = new GuestController;
$locations = $guestController->getLocations();
?>
<!DOCTYPE html>
<html>
<head>
    <title>Search | Find A physician</title>
    <link rel="stylesheet" href="assets/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="assets/css/card.css"/>


</head>
<body>

<div align="center" style="padding-top: 30px">

    <nav class="navbar navbar-inverse navbar-fixed-top" >
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand active" href="/">Find A Physician</a>
            </div>
            <ul class="nav navbar-nav">
                <li class="active"><a href=/>QuickSearch</a></li>
                <li class=""><a href="/catalogue.php">Catalogue</a></li>
                <li class=""><a href="/doctors">For Doctors</a></li>
                <!--<li><a href="#">Page 1</a></li>-->

            </ul>
            <ul class="nav navbar-nav navbar-right">
               <!-- <li><a href="#"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>-->
                <li><a href="/admin/index.php"><span class="glyphicon glyphicon-log-in"></span> Admin</a></li>
            </ul>
        </div>
    </nav>
</div>

<div class="container" style="padding-top: 5px">
    <div class="page-header">
        <h1>QuickSearch <small>Find a specialist physician in your location</small></h1>
       
    </div>

    <div class="row"  align="center" style="margin-top: 10%">

            <div class="input-group" style="width: 700px">
                <form action="API/V1/search.php" method="POST" role="form">
                    <div class="row">
                        <div class="col-md-5 col-sm-5">
                            <label>Search by Specialization</label>
                            <input type="text" placeholder="Specialization" class="form-control" id="specialization" name="specialization" REQUIRED autofocus><br />
                            <input type="hidden" name="web" value="1" >
                            <input type="hidden" name="type" value="specialization" >

                        </div>
                        <div class="col-md-5 col-sm-5">
                            <label>Location</label>

                            <select class="form-control" id="location" name="location" required>
                                <?php foreach ($locations as $location)
                                    echo "
                                 <option value='{$location['id']}'>{$location['name']}</option>
                                "
                                ?>
                            </select>


                        </div>
                        <div class="col-md-2 cl-sm-2"><br>
                        <button class="btn btn-success" id="search" value="submit"> Search</button>
                        </div>
                        

                    </div>
                   

                    <hr>





                </form>




            </div><!-- /input-group -->
        <!--<div class="input-group" style="width: 700px" >
                <form action="API/V1/search.php" method="POST" role="form">
                    <div class="row">
                        <div class="col-md-5 col-sm-5">
                            <label>Search by Illness</label>
                            <input type="text" placeholder="Illness" class="form-control" id="illness" name="illness" REQUIRED autofocus><br />
                            <input type="hidden" name="web" value="1" >
                            <input type="hidden" name="type" value="illness" >

                        </div>
                        <div class="col-md-5 col-sm-5">
                            <label>Location</label>

                            <select class="form-control" id="location" name="location" required>
                                <?php foreach ($locations as $location)
                                    echo "
                                 <option value='{$location['id']}'>{$location['name']}</option>
                                "
                                ?>
                            </select>


                        </div>
                        <div class="col-md-2 cl-sm-2"><br>
                        <button class="btn btn-success" id="search" value="submit"> Search</button>
                        </div>

                    </div>

                    <hr>





                </form>




            </div>--><!-- /input-group -->
         

   </div><!-- /.row -->
      <div class="list-group">
          <?php session_start();?>
          <?php if ($_SESSION['nothing']) echo $_SESSION['nothing']?>
          <?php if ($_SESSION['doctors'])?>

          <?php foreach ($_SESSION['doctors'] as $doctor )
              echo "<div class='card ' >
              <div class='container-fluid'>
                    <div class= 'col-md-7'>
                        <h2>{$doctor['firstName']} {$doctor['lastName']}</h2>
                        <strong>Specialization:</strong> {$doctor['specialization']}
                        <p>{$doctor['description']}</p> 
                    </div>
                    <div class= 'col-md-5'><br>
                      <div class= 'well'>
                        <h4>Hospital Infomation</h4>
                        <p> {$doctor['hospital']}</p>
                        <p><strong>Phone:</strong> {$doctor['hospitalPhone']}</p>
                        <p><strong>Address:</strong> {$doctor['address']}</p>
                        <p><strong>Locate:</strong><a class='glyphicon glyphicon-map-marker' href='http://www.google.com/maps/place/{$doctor['coordinates']}'>Maps</a></p>
                        
                      </div>
                      
                    </div>
              </div>
                   
                   
                    
                    
             </div><br>
            ";

          ?>

      </div>

</div>







<script src="assets/js/jquery-3.2.0.min.js"></script>

<script src="assets/js/bootstrap.min.js" ></script>
<script scr="assets/js/main.js"></script>
<script>

</script>
</body>


</html>